var pjson = require('../../package.json');
import dotenv from 'dotenv';
dotenv.config();

export const config = {
    server: {
        env: process.env.ENV,
        port: process.env.SERVER_PORT,
        apiVersion: process.env.API_VERSION,
        apiPath: process.env.API_PATH,
    },
    mongo:{
        uri: process.env.DB_URI,
        name: process.env.DB_NAME,
    },
    jwt:{
        secret: process.env.JWT_SECRET,
        public: process.env.JWT_PUBLIC
    },
    paths: {
        logger: process.env.LOGGER_PATH
    },
    package: pjson,
    mail: {
        host: process.env.MAIL_HOST,
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
        port: process.env.MAIL_PORT,
        from: process.env.MAIL_FROM,
    },
};
