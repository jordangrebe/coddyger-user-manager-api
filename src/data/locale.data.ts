import { ProfileLocale, UserLocale } from "./locales"

export const locale = {
    router: {
        idRequired: "La référence de l'objet est requise",
        nameRequired: "Le nom de l'enregistrement est requis",
        descriptionRequired: "La description de l'enregistrement est requise",
        statusRequired: "Le statut de l'enregistrement est requis",
    },
    controller: {
        notFound: "Enregistrement introuvable",
        wrongObjectId: "L'identifiant de l'objet n'est pas valide",
        saverNotfound: "Impossible de vérifier l'identité de l'administrateur, veuillez vous reconnecter. Si le problème persiste contactez un administrateur système.",
        subcategoryNotfound: "Sous-categorie introuvable",
        successSave: "Enregistrement réussie!",
        successUpdate: "Modification réussie!",
        successRemove: "Enregistrement supprimé avec succès.",
        apiKeyNotFound: "Clé d'api introuvable",
        notValidStatus: "Statut invalide",
        session: {
            ok: 'Session ok',
            error: 'Session invalide'
        },
        done: "Action éffectuée avec succès",

        application: {
            router: {
                nameRequired: "Le nom de l'enregistrement est requis"
            }
        },
        user: {
            passwordMismatch: "Le mot de passe et la confirmation sont différents"
        }
    },
    notfound: (label : string = '') => {
        return `${label} introuvable`
    },
    required: (label : string = '') => {
        return `${label} est un champs requis`
    },
    exist: (label : string = '') => {
        return `Un enregistrement avec ${label} exist déjà`
    },
    system: {
        errorTryCatchMessage: "Une erreur inattendue s'est produite."
    },

    user: UserLocale,
    profile: ProfileLocale,
}
