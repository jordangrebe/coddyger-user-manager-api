export const ProfileLocale = {
    router: {
        idRequired: "La référence de l'objet est requise",
        titleRequired: "Le titre est requis",
        codeRequired: "Le code est requis",
        statusRequired: "Le statut de l'enregistrement est requis",
        abilityRequired: "Veuilez sélectionner au mmoins un fonctionnalité",
    },
    controller: {
        wrongObjectId: "L'identifiant de l'objet n'est pas valide",
        saverNotfound: "Impossible de vérifier l'identité de l'administrateur, veuillez vous reconnecter. Si le problème persiste contactez un administrateur système.",
        subcategoryNotfound: "Sous-categorie introuvable",
        successSave: "Enregistrement réussie!",
        successUpdate: "Modification réussie!",
        successRemove: "Enregistrement supprimé avec succès.",
        notValidStatus: "Statut invalide",
        abilityNotfound: "Ressource introuvable",
        codeNotfound: "Code de profil est introuvable",
        codeOwned: "Ce code de profil a déjà été attribué",
        isOwned: "Ce profil existe déjà",
        session: {
            ok: 'Session ok',
            error: 'Session invalide'
        },
        done: "Action éffectuée avec succès",
    },
}