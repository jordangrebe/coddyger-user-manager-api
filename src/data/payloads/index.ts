import * as MessagesPayload from './message.json';
import * as CategoryPayload from './category.json';

export {
    MessagesPayload,
    CategoryPayload
}
