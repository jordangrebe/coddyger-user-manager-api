import { MailHelper } from "../../helpers";
import { config } from "../../utils";

export const MailTemplate = {
    twoFactorAuthentication: (uri:string): string => {
        return `<section style="background-color: ${MailHelper.templateColors};padding: 40px;">
        <h1 style="Margin: 0;background-color: ${MailHelper.templateColors};font-size: 25px;font-weight: 100;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;mso-line-height-rule: exactly;line-height: 50px;color: #fff;text-align: center;width: 100%">
            Authentification OTP
        </h1>
    </section>

        <section style="padding: 15px;border-spacing: 0;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;color: rgb(38, 38 ,38);border-collapse: collapse;background-color: #fff;font-size: 14px;line-height: 18px;">
        <p style="color: #000000">
        Bonjour, votre compte est en attente d'activation.<br />
        Pour cela vous devez cliquer sur ce <a href="${uri}">lien</a> pour l'activer et créer un mot de passe.<br />
        Utilisez le lien ci-dessous pour pour l'activation.<br/>
        </p>
        <p style="padding: 10px;background-color: #F0F0F0; color: #000000">
            Lien d'activation :: <span style="border-bottom: 1px dotted #333;">${uri}</span>
        </p>
        <p style="color: red; font-size: 12px">
        Si vous n'êtes pas l'auteur de cette demande, merci d'ignorer ce message.
        </p>
    </section>`;
    },
    welcomeMessage: (user:string): string => {
        return `<section style="background-color: ${MailHelper.templateColors};padding: 40px;">
        <h1 style="Margin: 0;background-color: ${MailHelper.templateColors};font-size: 25px;font-weight: 100;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;mso-line-height-rule: exactly;line-height: 50px;color: #fff;text-align: center;width: 100%">
            Activation de compte
        </h1>
    </section>

        <section style="padding: 15px;border-spacing: 0;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;color: rgb(38, 38 ,38);border-collapse: collapse;background-color: #fff;font-size: 14px;line-height: 18px;">
        <p style="color: #000000">
        Bonjour ${user}, votre compte chez ${config.package.description} est maintenant actif.<br />
        Vous pouvez vous connecter à présent avec vos identifiant:
        </p>
        <p style="padding: 10px;background-color: #F0F0F0;">
            Adresse e-mail :: <span style="border-bottom: 1px dotted #333;">${user}</span><br />
            mot de passe :: <span style="border-bottom: 1px dotted #333;">************</span>
        </p>
    </section>`;
    },
    editEmailOtp: (code:string | number): string => {
        return `<section style="background-color: ${MailHelper.templateColors};padding: 40px;">
        <h1 style="Margin: 0;background-color: ${MailHelper.templateColors};font-size: 25px;font-weight: 100;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;mso-line-height-rule: exactly;line-height: 50px;color: #fff;text-align: center;width: 100%">
            Modification d'adresse e-mail
        </h1>
    </section>

        <section style="padding: 15px;border-spacing: 0;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;color: rgb(38, 38 ,38);border-collapse: collapse;background-color: #fff;font-size: 14px;line-height: 18px;">
        <p style="color: #000000">
        Bonjour, utiliser ce code à deux facteurs pour continuer le processus de modification de votre compte ${config.package.description}.
        </p>
        <p style="padding: 10px;background-color: #F0F0F0; color: #000000">
            Code d'activation :: <span style="border-bottom: 1px dotted #333;">${code}</span>
        </p>
        <p style="color: red; font-size: 12px">
        Si vous n'êtes pas l'auteur de cette demande, merci d'ignorer ce message.
        </p>
    </section>`;
    },
    createPasswordUri: (code:string | number): string => {
        return `<section style="background-color: ${MailHelper.templateColors};padding: 40px;">
        <h1 style="Margin: 0;background-color: ${MailHelper.templateColors};font-size: 25px;font-weight: 100;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;mso-line-height-rule: exactly;line-height: 50px;color: #fff;text-align: center;width: 100%">
            Modification d'adresse e-mail
        </h1>
    </section>

        <section style="padding: 15px;border-spacing: 0;font-family: 'Segoe UI', Helvetica, Arial, sans-serif;color: rgb(38, 38 ,38);border-collapse: collapse;background-color: #fff;font-size: 14px;line-height: 18px;">
        <p style="color: #000000">
        Bonjour, utiliser ce code à deux facteurs pour continuer le processus de modification de votre compte ${config.package.description}.
        </p>
        <p style="padding: 10px;background-color: #F0F0F0; color: #000000">
            Code d'activation :: <span style="border-bottom: 1px dotted #333;">${code}</span>
        </p>
        <p style="color: red; font-size: 12px">
        Si vous n'êtes pas l'auteur de cette demande, merci d'ignorer ce message.
        </p>
    </section>`;
    }
} 
