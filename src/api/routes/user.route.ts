import {Router} from 'express';
import {routeDecorator} from './route.decorators';
import {cdg} from '../../utils';
import { UserController } from '../controllers';
import { userValidator } from '../middlewares/payloads';
import { JwtMiddleware, ValidatorMiddleware } from '../middlewares';

const Controller = new UserController()
const route = Router();
const routePath: string = '/user'

route.post(`${routePath}/generate-token`, userValidator.accessToken(), ValidatorMiddleware.validate, (req: any, res: any) => {
    const body = req.body

    let Q = Controller.generateToken(body)

    return cdg.api(res, Q);
});

route.post(`${routePath}/generate-password`, (req: any, res: any) => {
    let password:any = req.body.password

    let Q = Controller.generatePassword(password)

    return cdg.api(res, Q);
});

route.post(`${routePath}/verify-token`, JwtMiddleware.verify, (req: any, res: any) => {
    const body = req.user

    let Q = Controller.verifyToken(body)

    return cdg.api(res, Q);
});

route.post(`${routePath}/login`, JwtMiddleware.verifyAuth, userValidator.login(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;

    let Q = Controller.login(body)

    return cdg.api(res, Q);
});

route.post(`${routePath}/save`, JwtMiddleware.verify, userValidator.save(), ValidatorMiddleware.validate,  async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    let files:Array<any> = req.files;
    
    body.user = user._id;
    body.files = files;

    let Q = Controller.save(body)

    return cdg.api(res, Q);
});

route.put(`${routePath}/edit`, JwtMiddleware.verify, userValidator.update(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    
    body.user = user._id;
    let Q = Controller.update(body);

    return cdg.api(res, Q)
});

route.put(`${routePath}/edit-status`, JwtMiddleware.verify, userValidator.updateStatus(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    
    body.user = user._id;
    let Q = Controller.updateStatus(body);

    return cdg.api(res, Q)
});

route.get(`${routePath}/select/:payload?`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload
    let Q = Controller.select(payload)

    return cdg.api(res, Q);
});

route.get(`${routePath}/select-by-status/:payload`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload
    let Q = Controller.selectByStatus(payload)

    return cdg.api(res, Q);
});

route.get(`${routePath}/search/:payload`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload;
    let page: any = req.query.page || 1;

    let Q = Controller.search(payload, page);

    return cdg.api(res, Q)
});

route.delete(`${routePath}/remove/:_id`, JwtMiddleware.verify, async (req : any, res : any) => {
    let _id: any = req.params._id
    let user:any = req.user;

    let body: any = {};

    body._id = _id;
    body.user = user._id;
    let Q = Controller.remove(body)

    return cdg.api(res, Q);
});

route.put(`${routePath}/restore`, JwtMiddleware.verify, userValidator.restore(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;

    body.user = user._id;
    let Q = Controller.restore(body)

    return cdg.api(res, Q);
});

export class UserRoute {
    @routeDecorator(route)
    static router : any;
    constructor() {}
}
