import {Router} from 'express';
import {routeDecorator} from './route.decorators';
import {cdg} from '../../utils';
import { ProfileController } from '../controllers';
import { profileValidator } from '../middlewares/payloads';
import { JwtMiddleware, ValidatorMiddleware } from '../middlewares';

const Controller = new ProfileController()
const route = Router();
const routePath: string = '/profile'

route.post(`${routePath}/save`, JwtMiddleware.verify, profileValidator.save(), ValidatorMiddleware.validate,  async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    let files:Array<any> = req.files;
    
    body.user = user._id;
    body.files = files;

    let Q = Controller.save(body)

    return cdg.api(res, Q);
});

route.put(`${routePath}/edit`, JwtMiddleware.verify, profileValidator.update(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    
    body.user = user._id;
    let Q = Controller.update(body);

    return cdg.api(res, Q)
});

route.put(`${routePath}/edit-status`, JwtMiddleware.verify, profileValidator.updateStatus(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;
    
    body.user = user._id;
    let Q = Controller.updateStatus(body);

    return cdg.api(res, Q)
});

route.get(`${routePath}/select/:payload?`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload
    let Q = Controller.select(payload)

    return cdg.api(res, Q);
});

route.get(`${routePath}/select-by-status/:payload`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload
    let Q = Controller.selectByStatus(payload)

    return cdg.api(res, Q);
});

route.get(`${routePath}/search/:payload`, JwtMiddleware.verify, async (req : any, res : any) => {
    let payload: any = req.params.payload;
    let page: any = req.query.page || 1;

    let Q = Controller.search(payload, page);

    return cdg.api(res, Q)
});

route.delete(`${routePath}/remove/:_id`, JwtMiddleware.verify, async (req : any, res : any) => {
    let _id: any = req.params._id
    let user:any = req.user;

    let body: any = {};

    body._id = _id;
    body.user = user._id;
    let Q = Controller.remove(body)

    return cdg.api(res, Q);
});

route.put(`${routePath}/restore`, JwtMiddleware.verify, profileValidator.restore(), ValidatorMiddleware.validate, async (req : any, res : any) => {
    let body: any = req.body;
    let user:any = req.user;

    body.user = user._id;
    let Q = Controller.restore(body)

    return cdg.api(res, Q);
});

export class ProfileRoute {
    @routeDecorator(route)
    static router : any;
    constructor() {}
}
