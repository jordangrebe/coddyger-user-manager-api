import express, {Router} from 'express';
import {routeDecorator} from './route.decorators';
import {cdg, config} from '../../utils';
import { MainController } from '../controllers';
import { mainValidator } from '../middlewares/payloads';
import { JwtMiddleware, MulterMiddleware, ValidatorMiddleware } from '../middlewares';
import path from 'path';
import fs from 'fs';
import mime from 'mime';


const Controller = new MainController()

const route = Router();

route.get('/container', (req : any, res : any) => {
    let Q = new Promise((resolve, reject) => {

        resolve({status: 201, message: 'Service is Up', data: {
            name: config.package.name,
            version: config.package.version,
            description: config.package.description,
        }});
    })
    return cdg.api(res, Q);
});
route.get('/test/:action', async (req : any, res : any) => {
    let params = req.params
    let Q = Controller.test(params)

    return cdg.api(res, Q);
});

route.get('/select-file/:filename', async (req : any, res : any) => {
    let filename: string = req.params.filename;
    let filePath = MulterMiddleware.uploadPath + filename;

    // let file:any = fs.readFileSync(filePath);
    // const fileExtension = path.extname(filePath);
    const mimeType = mime.getType(filePath);

    fs.exists(filePath, function (exist: any) {
        if (! exist) { // if the file is not found, return 404
            res.statusCode = 404;
            res.end(`Fichier ${filePath} introuvable!`);
            return;
        }

        // if is a directory, then look for index.html
        if (fs.statSync(filePath).isDirectory()) {
            filePath += '/index.html';
        }

        // read file from file system
        fs.readFile(filePath, function (err, data) {
            if (err) {
                res.statusCode = 500;
                res.end(`Error getting the file: ${err}.`);
            } else { // if the file is found, set Content-type and send data
                res.setHeader('Content-type', mimeType);
                res.end(data);
            }
        });
    });
});




export class MainRoute {
    @routeDecorator(route)
    static router : any;
    constructor() {}
}
