import {Define, cdg, config} from '../../utils'
import {LoggerCore} from '../../core';
import {locale} from '../../data';
import { LoginInterface, UserInterface } from '../../models/interface';
import { ProfileSet, UserSet } from '../../models';
import { UserHelper } from '../../helpers';
import { JwtMiddleware } from '../middlewares';

export class UserController {
    save(payloads : UserInterface) {
        return new Promise(async (resolve, reject) => {
            let _id:any = payloads._id;
            let email:string = payloads.email;
            let password:string = payloads.password;
            let passwordConfirmation:string = payloads.passwordConfirmation;
            let profile:string = payloads.profile;
            let user:string = payloads.user!;

            // Controller l'existence de l'utilisateur
            let isUser: any = await UserSet.exist({_id: user});

            if (!isUser) {
                return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
            } else {
                UserSet.exist({email}).then(async (a:any) => {
                    if(a.error) {
                        reject(a)
                    } else {
                        let isProfile:any = await ProfileSet.exist({_id: profile});
                        let isEmail:any = await UserSet.exist({email});

                        if(!isProfile) {
                            return resolve({status: Define.clientErrorCode, message: locale.notfound("Profil"), data: {profile}})
                        } else if(isEmail) {
                            return resolve({status: Define.clientErrorCode, message: locale.exist("cette adresse e-mail"), data: {email}})
                        } else if(password != passwordConfirmation) {
                            return resolve({status: Define.clientErrorCode, message: locale.controller.user.passwordMismatch, data: null})
                        } else {
                            let ID: any = _id;
                            if (cdg.string.is_empty(_id)) {
                                ID = cdg.string.generateObjectId()
                            }
                            payloads._id = ID;
                            payloads.password = await cdg.encryptPassword(password)
            
                            // Enregistre historique de statut de la demande
                            UserSet.save(payloads).then(async (b : any) => {
                                if (b.hasOwnProperty('error')) {
                                    return reject(b)
                                } else {
                                    UserSet.selectOne({_id: ID}).then(async (c : any) => {
                                        resolve({status: Define.clientSuccessCode, message: locale.controller.successSave, data: c})
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'save'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })

    }

    update(payloads : UserInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            
            // Vérifier si la référence est de type ObjectID
            if(!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                let user:string = payloads.user!;
                let email:string = payloads.email;
                let contact:string = payloads.contact;

                if (!cdg.string.isValidObjectId(user)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
                } else { 
                    // Controller l'existence de l'utilisateur et de l'enregistrement
                    let isUser: any = await UserSet.exist({_id: user});
                    let isData: any = await UserSet.exist({_id});
                    let isEmail: any = await UserSet.exist({email});
                    let ownEmail:any = await UserSet.ownData({
                        $and: [
                            {email},
                            {_id},
                        ]
                    })

                    let isContact: any = await UserSet.exist({contact});
                    let ownContact:any = await UserSet.ownData({
                        $and: [
                            {contact},
                            {_id},
                        ]
                    })

                    if (!isUser) {
                        return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
                    } else if (! isData) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else if (isEmail && !ownEmail) {
                        return resolve({status: Define.clientErrorCode, message: locale.exist('cette adresse e-mail'), data: _id})
                    } else if (isContact && !ownContact) {
                        return resolve({status: Define.clientErrorCode, message: locale.exist('ce numero de téléphone'), data: _id})
                    } else { 
                        delete payloads._id;

                        UserSet.update({
                            _id
                        }, payloads).then((a : any) => {
                            if (a.hasOwnProperty('error')) {
                                reject(a)
                            } else { // Recupération des données de l'enregistrement en cours
                                UserSet.selectOne({_id}).then(async (b : any) => {
                                    payloads._id = _id;

                                    return resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: b})
                                })
                            }
                        })
                    }
                }
            }
        }).catch((err) => {
            console.error('UPDATE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'update'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    updateStatus(payloads : UserInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let status: string = payloads.status!;
            
            // Vérifier si la référence est de type ObjectID
            if(!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                let user: string = payloads.user!;

                if (!cdg.string.isValidObjectId(user)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
                } else { 
                    // Controller l'existence de l'utilisateur et de l'enregistrement
                    let isUser: any = await UserSet.exist({_id: user});
                    let isData: any = await UserSet.exist({_id});

                    if (!isUser) {
                        return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
                    } else if (! isData) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else { 
                        delete payloads._id

                        UserSet.update({
                            _id
                        }, {status}).then((Q : any) => {
                            if (Q.hasOwnProperty('error')) {
                                reject(Q)
                            } else {
                                // Recupération des données de l'enregistrement en cours
                                UserSet.selectOne({_id}).then(async (item : any) => {
                                    payloads._id = _id;

                                    return resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: item})
                                })
                            }
                        })
                    }
                }
            }
        }).catch((err) => {
            console.error('updateStatus CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'updateStatus'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    select(payload? : string) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_empty(payload)) {
                if(!cdg.string.isValidObjectId(payload)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {payload}})
                } else {
                    let _id = cdg.string.toObjectId(payload)
                    UserSet.selectOne({$and: [{_id}]}).then(async (data : any) => {
                        if (data) {
                            delete data.password
                            resolve({status: Define.clientSuccessCode, message: 'ok', data: data})
                        } else {
                            resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: {}})
                        }
                    });
                }
            } else {
                let Q: any = await UserSet.select({params: {status: 'active'}});
                let global: any = [];
                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        delete item.password

                        global.push(item)
                    }
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                } else {
                    reject(Q)
                }
            }
        }).catch((err) => {
            console.error('SELECT CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'select'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    selectByStatus(payload : number) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_number(payload)) {
                let Q: any = await UserSet.select({params: {status: payload}});
                let global: any = [];
                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        delete item.password

                        global.push(item)
                    }
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                } else {
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                }
            } else {
                resolve({status: Define.clientErrorCode, message: locale.controller.notValidStatus, data: null})
            }
        }).catch((err) => {
            console.error('selectByStatus CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'selectByStatus'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    search(payload : string, page:number) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_empty(payload)) {
                let Q: any = await UserSet.select({params: {$or: [{name: { $regex: payload }}]}});
                let global: any = []

                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        delete item.password

                        if(item) {
                            global.push(item)
                        }
                    }
                    let totalPaginate:any = cdg.paginate(global, 10, page);

                    resolve({status: Define.clientSuccessCode, message: {total: global.length, totalPerPage: totalPaginate.length, totalList: Math.round(global.length / 20)}, data: totalPaginate})
                } else {
                    reject(Q)
                }
            } else {
                
            }
        }).catch((err) => {
            console.error('SELECT CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'select'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    remove(payloads : UserInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let user: any = payloads.user;

            // Vérifier si les références sont de type ObjectID
            if (!cdg.string.isValidObjectId(user)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
            } else if (!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                // Controller l'existence de l'enregistrement
                UserSet.exist({_id}).then(async (Q) => {
                    if (!Q) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else {
                        // Controller l'existence de l'utilisateur et de l'enregistrement
                        let isUser: any = await UserSet.exist({_id: user});

                        if (! isUser) {
                            return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: {
                                    user
                                }})
                        } else {
                            UserSet.update({_id}, {status: 'removed'}).then((remove : any) => {
                                if (remove.error) {
                                    reject(remove)
                                } else {
                                    resolve({status: Define.clientSuccessCode, message: locale.controller.successRemove, data: null});
                                }
                            })
                        }
                    }
                })
            }

        }).catch((err) => {
            console.error('RESTORE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'restore'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    restore(payloads : UserInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let user: any = payloads.user;

            if (!cdg.string.isValidObjectId(user)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
            } else if (!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                UserSet.exist({_id}).then(async (Q) => {
                    if (!Q) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else {
                        let isUser: any = await UserSet.exist({_id: user});

                        if (! isUser) {
                            return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: {
                                    user
                                }})
                        } else {
                            UserSet.update({
                                _id
                            }, {status: 'active'}).then((update : any) => {
                                if (update.hasOwnProperty('error')) {
                                    reject(update)
                                } else {
                                    UserSet.selectOne({_id}).then(async (selectOne : any) => {
                                        resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: selectOne})
                                    })
                                }
                            })
                        }
                    }
                })
            }

        }).catch((err) => {
            console.error('RESTORE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'restore'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    login(payloads: LoginInterface): Promise<any> {
        return new Promise((resolve, reject) => {
            let email:string = payloads.email;
            let password:string|number = payloads.password;

            UserSet.exist({email}).then((a:any) => {
                if(a.error) {
                    reject(a)
                } else {
                    if(a === false) {
                        resolve({ status: Define.clientErrorCode, message: locale.notfound('Compte'), data: null })
                    } else {
                        UserSet.selectOne({email}).then(async (b:any) => {
                            if(b.error) {
                                reject(b)
                            } else {
                                let verifyPassword = await cdg.verifyPassword(password, b.password);
    
                                if (!verifyPassword) {
                                    resolve({
                                        status: Define.clientErrorCode,
                                        message: locale.user.controller.loginFailed,
                                        data: {
                                            msg: 'Mot de passe invalide'
                                        }
                                    })
                                } else if (b.status == 'suspended') {
                                    resolve({
                                        status: Define.clientErrorCode,
                                        message: locale.user.controller.suspendedAccount,
                                        data: null
                                    })
                                } else if (b.status == 'removed') {
                                    resolve({
                                        status: Define.clientErrorCode,
                                        message: locale.user.controller.removedAccount,
                                        data: null
                                    })
                                } else if (b.status == 'active') {
                                    b.password = null;
                                    
                                    UserHelper.buildForeigner(b).then(async (c:any) => {
                                        if(c.error) {
                                            reject(c)
                                        } else {
                                            await UserSet.update({email}, {lastLoginDate: new Date})
                                            // generate an access token
                                            const token = JwtMiddleware.generate({_id: b._id, email: b.email, ability: b.ability});

                                            resolve({
                                                status: Define.clientSuccessCode,
                                                message: locale.user.controller.loginSucceed,
                                                data: {
                                                    user: c,
                                                    accessToken: token
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    resolve({
                                        status: Define.clientErrorCode,
                                        message: locale.user.controller.badStatus,
                                        data: {status: b.status}
                                    })
                                }
                            }
                        })
                    }
                }
            });
        })
    }

    generateToken(payloads : any) {
        return new Promise(async (resolve, reject) => {
            resolve(await UserHelper.generateToken(payloads))
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'generateToken'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }
    verifyToken(payloads : any) {
        return new Promise(async (resolve, reject) => {
            resolve(await UserHelper.verifyToken(payloads))
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'generateToken'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }
    generatePassword(payload : any) {
        return new Promise(async (resolve, reject) => {
            let x:any = await cdg.encryptPassword(payload);
            resolve({
                status: Define.clientErrorCode,
                message: locale.user.controller.done,
                data: {x}
            })
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserController', method: 'generatePassword'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }
}
