import {Define, cdg, config} from '../../utils'
import {LoggerCore} from '../../core';
import {locale} from '../../data';
import { ProfileInterface } from '../../models/interface';
import { ProfileSet, UserSet } from '../../models';
import { CategoryPayload } from '../../data/payloads';
import { UserHelper } from '../../helpers';

let Abilities = CategoryPayload.ability
let Codes = CategoryPayload.profileCode

export class ProfileController {
    save(payloads : ProfileInterface) {
        return new Promise(async (resolve, reject) => {
            let _id:any = payloads._id;
            let title: string = payloads.title;
            let ability: Array<any> = payloads.ability;
            let user:string = payloads.user!;

            // Controller l'existence de l'utilisateur
            let isUser: any = await UserSet.exist({_id: user});

            if (!isUser) {
                return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
            } else {
                let success = 0
                let failure = 0;
                // GET ABILITIES KEY INSIDE AN ARRAY OF STRING
                let abilityData = UserHelper.getAbilitiesKey(Abilities)
                for (let x in ability) {
                    let a: any = ability[x].subject;
                    // GET SINGLE ABILITY

                    // CHECK ABILITY EXIST IN JSON DATABASE
                    if (cdg.inArray(a, abilityData)) {
                        success += 1
                        continue
                    } else {
                        failure += 1
                        continue
                    }
                }

                // if (failure >= 1) {
                //     resolve({status: Define.clientErrorCode, message: `${locale.profile.controller.abilityNotfound}:${failure}`, data: ability});
                // } else {
                    
                // }

                ProfileSet.exist({title}).then((a:any) => {
                    if(a.error) {
                        reject(a)
                    } else {
                        if(a == true) {
                            return resolve({status: Define.clientErrorCode, message: locale.exist('ce titre'), data: {title}})
                        } else {
                            let ID: any = _id;
                            if (cdg.string.is_empty(_id)) {
                                ID = cdg.string.generateObjectId()
                            }
                            payloads._id = ID;
                            payloads.code = title.replace(/[^A-Z0-9]/ig, "_")
            
                            // Enregistre historique de statut de la demande
                            ProfileSet.save(payloads).then(async (b : any) => {
                                if (b.hasOwnProperty('error')) {
                                    return reject(b)
                                } else {
                                    ProfileSet.selectOne({_id: ID}).then(async (c : any) => {
                                        resolve({status: Define.clientSuccessCode, message: locale.controller.successSave, data: c})
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'save'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })

    }

    update(payloads : ProfileInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let title: string = payloads.title;
            
            // Vérifier si la référence est de type ObjectID
            if(!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                let user:string = payloads.user!;

                if (!cdg.string.isValidObjectId(user)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
                } else { 
                    // Controller l'existence de l'utilisateur et de l'enregistrement
                    let isUser: any = await UserSet.exist({_id: user});
                    let isData: any = await ProfileSet.exist({_id});
                    let isName: any = await ProfileSet.exist({title})
                    let ownName: any = await ProfileSet.ownData({$and: [{title}, {_id}]})

                    if (!isUser) {
                        return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
                    } else if (! isData) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else if (isName && !ownName) {
                        resolve({
                            status: Define.clientErrorCode,
                            message: locale.exist('ce titre'),
                            data: {title}
                        })
                    } else { 
                        delete payloads._id;

                        ProfileSet.update({
                            _id
                        }, payloads).then((Q : any) => {
                            if (Q.hasOwnProperty('error')) {
                                reject(Q)
                            } else { // Recupération des données de l'enregistrement en cours
                                ProfileSet.selectOne({_id}).then(async (item : any) => {
                                    payloads._id = _id;

                                    return resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: item})
                                })
                            }
                        })
                    }
                }
            }
        }).catch((err) => {
            console.error('UPDATE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'update'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    updateStatus(payloads : ProfileInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let status: string = payloads.status!;
            
            // Vérifier si la référence est de type ObjectID
            if(!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                let user: string = payloads.user!;

                if (!cdg.string.isValidObjectId(user)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
                } else { 
                    // Controller l'existence de l'utilisateur et de l'enregistrement
                    let isUser: any = await UserSet.exist({_id: user});
                    let isData: any = await ProfileSet.exist({_id});

                    if (!isUser) {
                        return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: user})
                    } else if (! isData) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else { 
                        delete payloads._id

                        ProfileSet.update({
                            _id
                        }, {status}).then((Q : any) => {
                            if (Q.hasOwnProperty('error')) {
                                reject(Q)
                            } else {
                                // Recupération des données de l'enregistrement en cours
                                ProfileSet.selectOne({_id}).then(async (item : any) => {
                                    payloads._id = _id;

                                    return resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: item})
                                })
                            }
                        })
                    }
                }
            }
        }).catch((err) => {
            console.error('updateStatus CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'updateStatus'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    select(payload? : string) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_empty(payload)) {
                if(!cdg.string.isValidObjectId(payload)) {
                    resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {payload}})
                } else {
                    let _id = cdg.string.toObjectId(payload)
                    ProfileSet.selectOne({$and: [{_id}]}).then(async (data : any) => {
                        if (data) {
                            resolve({status: Define.clientSuccessCode, message: 'ok', data: data})
                        } else {
                            resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: {}})
                        }
                    });
                }
            } else {
                let Q: any = await ProfileSet.select({params: {status: 'active'}});
                let global: any = [];
                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        global.push(item)
                    }
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                } else {
                    reject(Q)
                }
            }
        }).catch((err) => {
            console.error('SELECT CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'select'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    selectByStatus(payload : number) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_number(payload)) {
                let Q: any = await ProfileSet.select({params: {status: payload}});
                let global: any = [];
                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        global.push(item)
                    }
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                } else {
                    resolve({status: Define.clientSuccessCode, message: 'ok', data: global})
                }
            } else {
                resolve({status: Define.clientErrorCode, message: locale.controller.notValidStatus, data: null})
            }
        }).catch((err) => {
            console.error('selectByStatus CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'selectByStatus'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    search(payload : string, page:number) {
        return new Promise(async (resolve, reject) => {
            if (!cdg.string.is_empty(payload)) {
                let Q: any = await ProfileSet.select({params: {$or: [
                    {status: 'active'},
                    {title: { $regex: payload }},
                    {code: { $regex: payload }},
                ]}});
                let global: any = []

                if (Q) {
                    for (let x in Q) {
                        let item = Q[x];

                        if(item) {
                            global.push(item)
                        }
                    }
                    let totalPaginate:any = cdg.paginate(global, 10, page);

                    resolve({status: Define.clientSuccessCode, message: {total: global.length, totalPerPage: totalPaginate.length, totalList: Math.round(global.length / 20)}, data: totalPaginate})
                } else {
                    reject(Q)
                }
            } else {
                
            }
        }).catch((err) => {
            console.error('SELECT CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'select'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    remove(payloads : ProfileInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let user: any = payloads.user;

            // Vérifier si les références sont de type ObjectID
            if (!cdg.string.isValidObjectId(user)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
            } else if (!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                // Controller l'existence de l'enregistrement
                ProfileSet.exist({_id}).then(async (Q) => {
                    if (!Q) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else {
                        // Controller l'existence de l'utilisateur et de l'enregistrement
                        let isUser: any = await UserSet.exist({_id: user});

                        if (! isUser) {
                            return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: {
                                    user
                                }})
                        } else {
                            ProfileSet.update({_id}, {status: 'removed'}).then((remove : any) => {
                                if (remove.error) {
                                    reject(remove)
                                } else {
                                    resolve({status: Define.clientSuccessCode, message: locale.controller.successRemove, data: null});
                                }
                            })
                        }
                    }
                })
            }

        }).catch((err) => {
            console.error('RESTORE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'restore'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    restore(payloads : ProfileInterface) {
        return new Promise(async (resolve, reject) => {
            let _id: any = payloads._id;
            let user: any = payloads.user;

            if (!cdg.string.isValidObjectId(user)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {user}})
            } else if (!cdg.string.isValidObjectId(_id)) {
                resolve({status: Define.clientErrorCode, message: locale.controller.wrongObjectId, data: {_id}})
            } else {
                ProfileSet.exist({_id}).then(async (Q) => {
                    if (!Q) {
                        return resolve({status: Define.clientErrorCode, message: locale.notfound('Enregistrement'), data: _id})
                    } else {
                        let isUser: any = await UserSet.exist({_id: user});

                        if (! isUser) {
                            return resolve({status: Define.clientErrorCode, message: locale.controller.saverNotfound, data: {
                                    user
                                }})
                        } else {
                            ProfileSet.update({
                                _id
                            }, {status: 'active'}).then((update : any) => {
                                if (update.hasOwnProperty('error')) {
                                    reject(update)
                                } else {
                                    ProfileSet.selectOne({_id}).then(async (selectOne : any) => {
                                        resolve({status: Define.clientSuccessCode, message: locale.controller.successUpdate, data: selectOne})
                                    })
                                }
                            })
                        }
                    }
                })
            }

        }).catch((err) => {
            console.error('RESTORE CAUGHT')
            LoggerCore.log({type: 'error', content: err, location: 'ProfileController', method: 'restore'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }
}
