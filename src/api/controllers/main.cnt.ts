import {Define, cdg, config} from './../../utils'
import {LoggerCore} from '../../core';
import { MulterMiddleware } from '../middlewares';

export class MainController {
    test(payload : {
        action: string,
        body: any
    }) {
        return new Promise(async (resolve, reject) => {
            switch (payload.action) {
                case 'logger':
                    let q = await LoggerCore.log({type: 'error', content: 'lorem ipsum dolor', location: 'ma_fontion', method: 'test'})
                    resolve({status: 200, message: 'ok', data: q})
                    break;
                default:

            }
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'MainController', method: 'test'});
            return({status: Define.systemErrorCode, message: err.message, data: err})
        })
    }

    async _saveFiles(files: Array<any>): Promise<any> {
        try {
            return await new Promise(async (resolve, reject) => {
                let newFiles: Array<string> = [];
                for (let file of files) {
                    let save: any = await MulterMiddleware.save(file);

                    if (save.error) {
                        reject(save);
                    }

                    newFiles.push(save.filename)
                }

                resolve(newFiles);
            });
        } catch (err) {
            return { error: true, data: err };
        }
    }
}
