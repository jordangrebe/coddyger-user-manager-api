import {check} from "express-validator";
import { locale } from "../../../data";

export class profileValidator {
    static save() {
        return [
            check('title').not().isEmpty().withMessage(locale.profile.router.titleRequired),
            check('ability').not().isEmpty().withMessage(locale.profile.router.abilityRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
        ]
    }

    static updateStatus() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('status').not().isEmpty().withMessage(locale.router.statusRequired),
        ]
    }

    static restore() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
        ]
    }
}
