import {check} from "express-validator";
import { locale } from "../../../data";

export class userValidator {
    static save() {
        return [
            check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
            check('profile').not().isEmpty().withMessage(locale.user.router.profileRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
        ]
    }

    static accessToken() {
        return [
            check('apikey').not().isEmpty().withMessage(locale.user.router.apikeyRequired),
        ]
    }
    
    static login() {
        return [
            check("email").not().isEmpty().withMessage(locale.user.controller.loginFailed),
            check("password").not().isEmpty().withMessage(locale.user.controller.loginFailed),
        ]
    }

    static updateStatus() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('status').not().isEmpty().withMessage(locale.router.statusRequired),
        ]
    }

    static restore() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
        ]
    }
}
