import jwt from 'jsonwebtoken';
import {config, cdg, Define} from '../../utils';
export class JwtMiddleware {
    static verify(req: any, res: any, next: any) {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, config.jwt.secret!, (err: any, user: any) => {
                if (err) {
                    const errName = err.name;
                    let errLabel: string;
                    if(errName === 'TokenExpiredError') {
                        errLabel = 'Votre session a expiré'
                    } else if(errName === 'JsonWebTokenError') {
                        errLabel = 'Signature du token invalide'
                    } else {
                        errLabel = err
                    }
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: Define.apiAuthorisationErrorCode,
                            message: errLabel,
                            data: errLabel
                        });
                    }));
                }

                req.user = user;
                next();
            });
        } else {
            return cdg.api(res, new Promise((resolve)=>{
                resolve({
                    status: Define.apiAuthorisationErrorCode,
                    message: 'Accès non autorisé',
                    data: ''
                });
            }));
        }
    }
    static verifyAuth(req: any, res: any, next: any) {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, process.env.JWT_AUTH_SECRET!, (err: any, user: any) => {
                if (err) {
                    const errName = err.name;
                    let errLabel: string;
                    if(errName === 'TokenExpiredError') {
                        errLabel = 'Votre session a expiré'
                    } else if(errName === 'JsonWebTokenError') {
                        errLabel = 'Signature du token invalide'
                    } else {
                        errLabel = err
                    }
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: Define.apiAuthorisationErrorCode,
                            message: errLabel,
                            data: errLabel
                        });
                    }));
                }

                req.user = user;
                next();
            });
        } else {
            return cdg.api(res, new Promise((resolve)=>{
                resolve({
                    status: Define.apiAuthorisationErrorCode,
                    message: 'Accès auth non autorisé',
                    data: ''
                });
            }));
        }
    }
    static generate(data: any){
        // generate an access token - exp: Math.floor(Date.now() / 1000) + (60 * 60)
        return jwt.sign(data, config.jwt.secret!, { expiresIn: '720m' });
    }
    static generateAuth(data: {}){
        return jwt.sign(data, process.env.JWT_AUTH_SECRET!, { expiresIn: '720m' });
    }
    
}