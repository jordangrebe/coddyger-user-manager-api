import multer from 'multer'
import path from "path"
import fs from "fs"
import mime from 'mime-types'
import {cdg} from '../../utils'
import {MulterFileInterface} from '../../models/interface/file.interface'

// NATS SERVER
export class MulterMiddleware {
    static uploadPathTmp: any = path.join(cdg.root() + process.env.UPLOAD_TMP_PATH);
    static uploadPath: any = path.join(cdg.root() + process.env.UPLOAD_PATH);
    static maxFile = 10;
    static fileSize = 1000000 * 5;

    static async single(req: any, res: any, next:any) {
        await MulterMiddleware.buildUploadPath();
        let allowedExtension = ['pdf', 'xls', 'xlsx', 'docx', 'doc', 'png', 'jpg', 'jpeg'];
        const upload = multer({
            dest: MulterMiddleware.uploadPathTmp,
            fileFilter: (req:any, file:any, cb:any) => {
                if (!cdg.inArray(MulterMiddleware.buildExt(file.mimetype), allowedExtension)) {
                    req.fileValidationError = 'Type de fichier non autorisé';
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 422,
                            message: 'error',
                            data: cdg.buildApiError({msg: 'Type de fichier non autorisé'})
                        });
                    }));
                }
                cb(null, true);
            }
        }).single("file");

        upload(req, res, function (err: any) {
            if(req.file) {
                if (err instanceof multer.MulterError) {
                    if(err.code === 'LIMIT_FILE_SIZE') {
                        err.message = "Fichier trop volumineux"
                    } else if (err.code === 'LIMIT_FILE_COUNT') {
                        err.message = "Nombre maximum de fichier atteint"
                    }
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 401,
                            message: 'error',
                            data: cdg.buildApiError({msg: [err]})
                        });
                    }));
                } else if(req.fileValidationError) {
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 401,
                            message: 'error',
                            data: cdg.buildApiError({msg: req.fileValidationError})
                        });
                    }));
                } else if (err) {
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 401,
                            message: 'error',
                            data: cdg.buildApiError({msg: err.message})
                        });
                    }));
                }
            } else {
                req.file = '';
            }

            next()
        })
    }
    static async multiple(req: any, res: any, next:any) {
        await MulterMiddleware.buildUploadPath();
        let allowedExtension = ['pdf', 'xls', 'xlsx', 'docx', 'doc', 'png', 'jpg', 'jpeg'];
        const upload = multer({
            dest: MulterMiddleware.uploadPathTmp,
            limits: {
                fileSize: MulterMiddleware.fileSize,
                files: MulterMiddleware.maxFile,
            },
            fileFilter: (req:any, file:any, cb:any) => {
                if (!cdg.inArray(MulterMiddleware.buildExt(file.mimetype), allowedExtension)) {
                    req.fileValidationError = 'Type de fichier non autorisé';
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 422,
                            message: 'error',
                            data: cdg.buildApiError({msg: 'Type de fichier non autorisé'})
                        });
                    }));
                }
                cb(null, true);
            }
        }).array("files", MulterMiddleware.maxFile);

        upload(req, res, function (err: any) {
            if(req.files) {
                if (err instanceof multer.MulterError) {
                    if(err.code === 'LIMIT_FILE_SIZE') {
                        err.message = "Fichier trop volumineux"
                    } else if (err.code === 'LIMIT_FILE_COUNT') {
                        err.message = "Nombre maximum de fichier atteint"
                    }
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 422,
                            message: err.message,
                            data: cdg.buildApiError({msg: [err]})
                        });
                    }));
                } else if(req.fileValidationError) {
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 422,
                            message: 'error',
                            data: cdg.buildApiError({msg: req.fileValidationError})
                        });
                    }));
                } else if (err) {
                    return cdg.api(res, new Promise((resolve)=>{
                        resolve({
                            status: 500,
                            message: 'error',
                            data: cdg.buildApiError({msg: err.message})
                        });
                    }));
                }
            } else {
                req.files = [];
            }

            next()
        })
    }

    static save(file: MulterFileInterface) {
        return new Promise((resolve, reject) => {
            const tempPath = file.path;
            let fileExt = MulterMiddleware.buildExt(file.mimetype);
            let fullFilePath = MulterMiddleware.uploadPath + file.filename + '.' + fileExt;
            let filename:string = file.filename + '.' + fileExt;

            fs.rename(tempPath, fullFilePath, err => {
                if (err) reject(err);
    
                if(cdg.file.exists(tempPath)) {
                    fs.rmdirSync(tempPath);
                }
                resolve({filename, fullFilePath})
            });
        }).catch((error) => {
            return {error: true, data: error}
        })
    }
    static buildExt(mimetype: string) {
        return mime.extension(mimetype);
    }
    static async buildUploadPath() {
        // CREATE TEMPORY UPLOAD PATH
        if (!fs.existsSync(this.uploadPathTmp)) {
            await fs.promises.mkdir(this.uploadPathTmp, { recursive: true })
        }
        // CREATE UPLOAD PATH
        if (!fs.existsSync(this.uploadPath)) {
            await fs.promises.mkdir(this.uploadPath, { recursive: true })
        }
    }
}
