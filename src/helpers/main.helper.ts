import {MainSet} from '../models';
import { MulterMiddleware } from '../api/middlewares';
import { MulterFileInterface, MainInterface } from '../models/interface';
import { LoggerCore } from '../core';

export class MainHelper {
    static savePartnerFile(_id: any, file: MulterFileInterface): Promise < object > {
        return new Promise< object >((resolve, reject) => {
            // const uploadPath = '';
            MulterMiddleware.save(file).then((Q:any) => {
                if(Q.error) reject(Q);

                //
                file.uri = Q;

                MainSet.update({_id}, {file, filename: file.filename}).then((update:any) => {
                    if(update.error) reject(update);

                    resolve(Q);
                })
            })
        }).catch((err) => {
            LoggerCore.log({type: 'error', content: err, location: 'MainHelper', method: 'buildForeigner'});
            return {error: true, data: err}
        });
    }
}
