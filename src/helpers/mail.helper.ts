import nodemailer from 'nodemailer';
import { LoggerCore } from '../core';
import { locale } from '../data';
import { Define, config, cdg } from '../utils';

export class MailHelper {
    static templateColors: string = '#000000'
    static send(to: string, subject: string, body:string, from:string = '', fromName: string = '') {
        if(cdg.string.is_empty(from)) {
            from = config.mail.from!
        }if(cdg.string.is_empty(fromName)) {
            fromName = config.package.description
        }

        return new Promise((resolve, reject) => {
            var transporter = nodemailer.createTransport({
                host: config.mail.host,
                auth: {
                    user: config.mail.user,
                    pass: config.mail.pass
                },
                tls: {
                    ciphers:'SSLv3'
                }
            });
            
            const mailOptions = {
                from: {
                    name: fromName,
                    address: from
                }, // sender address
                to: to, // list of receivers
                subject: subject, // Subject line
                html: body// plain text body
            };
            
            transporter.sendMail(mailOptions, function (err: any, info: any) {
                if(err) {
                    reject(err)
                } else {
                    resolve(info)
                }
            })
        }).catch((err: any) => {
            LoggerCore.log({type: 'error', content: err, location: 'MailHelper', method: 'send'})
            return ({error: true, data:err, message: locale.system.errorTryCatchMessage})
        })
    }
    static template(body: string) {
        const header = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8" />
            <title>Coddyger Mail Template</title>
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        </head>
        <body style="background: #DEDEDE;">
        <style>
        
        </style>
        <div style="margin-left: auto;margin-right: auto;max-width: 650px;width: 100%;font-family: 'Segoe UI Light', Arial ,sans-serif; padding-top: 25px">
            <header style="background: #fff; padding: 10px;border-top: 1px solid ${this.templateColors};">
                <table style="width: 100%;">
                    <tr>
                        <td align="center" style="color: #000000">${config.package.description}</td>
                    </tr>
                </table>
            </header>
        `;
        const footer = `<footer style="text-align: center; padding: 10px">
                &copy; GS2E/DDI 2018 - All Rights Reserved
            </footer>
        </div>

        </body>
        </html>`;
        return header + body + footer
    }
}
