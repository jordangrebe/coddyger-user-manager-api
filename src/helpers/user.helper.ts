import { JwtMiddleware } from '../api/middlewares';
import { locale } from '../data';
import { CategoryPayload } from '../data/payloads';
import {UserSet, ProfileSet} from '../models';
import { ProfileInterface } from '../models/interface';
import { cdg, config, Define } from '../utils';
import { OtpHelper } from './otp.helper';

export class UserHelper {
    static buildForeigner(user : any) {
        return new Promise((resolve, reject) => {
            let item: any = user;

            ProfileSet.selectOne({_id: item.profile}).then((a:any) => {
                if(a.error) {
                    reject(a)
                } else {
                    let profile:any = a
                    let ability:any = profile.ability

                    item.profile = profile
                    item.ability = ability

                    resolve(item)
                }
            })
        }).catch((err : any) => {
            return({error: true, data: err})
        })
    }

    static getAbilitiesKey(ability : any) {
        let abilityKeyArray: any = [];
        for (let x in ability) {
            let a: any = ability[x];
            abilityKeyArray.push(a)
        }

        return abilityKeyArray
    }

    static generateToken(payload: any): Promise < string | object > {
        return new Promise(
            (resolve, reject) => { // GENERATE UNIQ ID FOR CURRENT SAVE
                let key = config.jwt.public
                let apikey = payload.apikey
    
                if (key !== apikey) {
                    resolve({
                        status: Define.clientErrorCode,
                        message: locale.controller.apiKeyNotFound,
                        data: {
                            apikey
                        }
                    })
                } else {
                    let token = JwtMiddleware.generateAuth({
                        data: apikey,
                        reg: new Date()
                    })
    
                    resolve({
                        status: Define.clientSuccessCode,
                        message: 'ok',
                        data: token
                    });
                }
            }
        )
    }
    static verifyToken(payload: any): Promise < string | object > {
        return new Promise(async (resolve, reject) => { // GENERATE UNIQ ID FOR CURRENT SAVE
                if (cdg.object.is_empty(payload)) {
                    resolve({
                        status: Define.clientErrorCode,
                        message: 'Session invalide',
                        data: payload
                    })
                } else {
                    delete payload.exp
                    delete payload.iat
        
                    let accessToken = await JwtMiddleware.generate(payload)
                    resolve({
                        status: Define.clientSuccessCode,
                        message: 'Session valide',
                        data: accessToken
                    })
                }
            }
        )
    }

    // Insert default user from sample
    static buildDefaultUser() {
        return new Promise<Object>(async(resolve, reject) => {
            this.buildDefaultProfile().then(async(profile: any) => {
                if(profile.error) {
                    return false;
                } else {
                    let users:any = await UserSet.select({})

                    if(users.length <= 0) {
                        users = CategoryPayload.defaultUser
                        
                        for(let u in users) {
                            let user:any = users[u];
                            let _id: any = cdg.string.generateObjectId();
                            let email:string = user.email;
                            let password:any = await cdg.encryptPassword('demo');

                            user._id = _id
                            user.profile = profile._id
                            user.user = null
                            user.isEmailConfirmed = true
                            user.password = password
        
                            if(!cdg.string.is_empty(email)) {
                                UserSet.save(user).then(async (Q : any) => {
                                    if (Q.hasOwnProperty('error')) {
                                        console.log('Sentinel::', `Default user '${email}' creation failed`)
                                    } else {
                                        // await transporter.save(JSON.stringify(user));

                                        // OtpHelper.sendOtpMail(email).then((O:any) => {
                                        //     if (O.hasOwnProperty('error')) {
                                        //         reject(O)
                                        //     } else {
                                        //         console.log('Sentinel::OTP', O)
                                        //     }
                                        // })
                                        
                                        console.log('Sentinel::', `Default user '${user.email}' created successfully`)
                                    }
                                }) 
                            }
                        }
                    }
                }
            })
        })
    }
    static buildDefaultProfile() {
        return new Promise<Object>(async(resolve, reject) => {
            ProfileSet.exist({code: 'master'}).then(async (Q : any) => {
                if(Q.error) {
                    reject(Q)
                } else {
                    if(!Q) {
                        let profiles:Array<any> = CategoryPayload.defaultProfile
                        profiles.forEach((profile:ProfileInterface) => {
                            let _id: any = cdg.string.generateObjectId();
                            profile._id = _id
    
                            ProfileSet.save(profile).then((save : any) => {
                                if (save.hasOwnProperty('error')) {
                                    console.log('Sentinel::', `Default profile '${profile.title}' creation failed`)
                                    reject(save)
                                } else {
                                    console.log('Sentinel::', `Default profile '${profile.title}' created successfully`)
                                    resolve(save)
                                }
                            })
                        })
                    } else {
                        let getProfile:any = await ProfileSet.selectOne({code: 'master'})
                        resolve(getProfile)
                    }
                }
            })
        }).catch((err) => {
            console.log('Sentinel::', `Default profile creation run on error`)
            return {error: true, data: err}
        })
    }
}
