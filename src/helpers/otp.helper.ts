import {LoggerCore} from '../core/logger.core';
import { locale } from '../data';
import { MailTemplate } from '../data/template';
import { OtpSet } from '../models';
import { OtpInterface } from '../models/interface';
import { cdg, config } from '../utils';
import { MailHelper } from './mail.helper';
const crypto = require('crypto')

const digits = '0123456789'
const lowerCase = 'abcdefghijklmnopqrstuvwxyz'
const upperCase = lowerCase.toUpperCase()
const specialChars = '#!&@'

export class OtpHelper {
    static generate(length : number = 6, options : {
        available_sets?: string
    }) {
        return new Promise((resolve, reject) => {
            let available_sets = options.available_sets || 'luds'

            let sets = [];
            if (available_sets.indexOf('l') > -1) {
                sets.push(lowerCase)
            }

            if (available_sets.indexOf('u') > -1) {
                sets.push(upperCase)
            }

            if (available_sets.indexOf('d') > -1) {
                sets.push(digits)
            }

            if (available_sets.indexOf('s') > -1) {
                sets.push(specialChars)
            }


            let all = sets.join('');
            let password = '';

            while (password.length<length) {
                const charIndex = crypto.randomInt(0, all.length)
                password += all[charIndex]
            }

            resolve(password)
        }).catch((err: any) => {
            LoggerCore.log({type: 'error', content: err, location: 'OtpHelper', method: 'generate'})
            return({error: true, data: err, message: locale.system.errorTryCatchMessage})
        }) 
    }
    static save(payloads: OtpInterface) {
        return new Promise(async (resolve, reject) => {
            let _id:any = cdg.string.generateObjectId();

            payloads._id = _id;

            // Modifier le status d'un existant active avant l'enregistrement d'un nouveau
            await OtpSet.update({$and: [{user: payloads.user}, {status: 'active'}]}, {status: 'used'});

            OtpSet.save(payloads).then((Q : any) => {
                if (Q.hasOwnProperty('error')) {
                    reject(Q)
                } else {
                    OtpSet.selectOne({_id}).then(async (Q:any) => {

                        resolve(Q)
                    })
                }
            })
        }).catch((err: any) => {
            LoggerCore.log({type: 'error', content: err, location: 'OtpHelper', method: 'save'})
            return({error: true, data: err, message: locale.system.errorTryCatchMessage})
        }) 
    }

    static sendWelcomeMail(user : string) {
        return new Promise((resolve, reject) => {
            let messagebody = MailTemplate.welcomeMessage(user); // Construire le corps du message avec l'adresse e-mail
            let accountActivationTemplate = MailHelper.template(messagebody); // Construire le message complet
            MailHelper.send(user, 'Compte ' + config.package.description + ' activé', accountActivationTemplate).then((Q : any) => {
                if (Q.hasOwnProperty('error')) {
                    reject(Q);
                } else {
                    resolve(Q);
                }
            })
        }).catch((err : any) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserHelper', method: 'isMobileNumber'})
            return({error: true, data: err, message: locale.system.errorTryCatchMessage})
        })
    }
    static sendOtpMail(email:string) {
        return new Promise(async (resolve, reject) => {
            let otpCode: any = await OtpHelper.generate(45, {available_sets: 'ld'}) // Générer code otp

            OtpHelper.save({user: email, code: otpCode, object: 'account_activation'}).then((O : any) => {
                if (O.hasOwnProperty('error')) {
                    reject(O)
                } else {
                    let uri = process.env.BACKOFFICE_URI + '/confirmation/password/' + otpCode
                    let messagebody = MailTemplate.twoFactorAuthentication(uri); // Construire le corps du message avec le code
                    let accountActivationTemplate = MailHelper.template(messagebody); // Construire le message complet
                    MailHelper.send(email, 'Activation de compte - ' + process.env.APP_NAME, accountActivationTemplate).then((Q : any) => {
                        if (Q.hasOwnProperty('error')) {
                            reject(Q);
                        } else {
                            Q.content = otpCode;
                            resolve(Q);
                        }
                    })
                }
            })
        }).catch((err : any) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserHelper', method: 'sendOtpMail'})
            return({error: true, data: err, message: locale.system.errorTryCatchMessage})
        })
    }
    static sendOtpMailEdit(email:string) {
        return new Promise(async (resolve, reject) => {
            let otpCode: any = await OtpHelper.generate(6, {available_sets: 'd'}) // Générer code otp

            OtpHelper.save({user: email, code: otpCode, object: 'edit_email'}).then((O : any) => {
                if (O.hasOwnProperty('error')) {
                    reject(O)
                } else {
                    let messagebody = MailTemplate.editEmailOtp(otpCode); // Construire le corps du message avec le code
                    let accountActivationTemplate = MailHelper.template(messagebody); // Construire le message complet
                    MailHelper.send(email, 'Modification d\'adresse e-mail ' + config.package.description + '', accountActivationTemplate).then((Q : any) => {
                        if (Q.hasOwnProperty('error')) {
                            reject(Q);
                        } else {
                            resolve(Q);
                        }
                    })
                }
            })
        }).catch((err : any) => {
            LoggerCore.log({type: 'error', content: err, location: 'UserHelper', method: 'isMobileNumber'})
            return({error: true, data: err, message: locale.system.errorTryCatchMessage})
        })
    }
}
