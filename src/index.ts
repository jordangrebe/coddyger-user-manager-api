import express from 'express';
import helmet from 'helmet';
import cors from 'cors';

import {cdg, config, Define} from './utils';
import { setRoutes } from './api/routes/route.decorators';
import './api/routes';
import { Mongoose } from './models/db';
import { UserHelper } from './helpers';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();

        this.config();
        this.routing();
        this.listen();
    }

    private config(): void {
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(express.json());
        this.app.use(express.static('public/uploads'));
    }

    private routing(): void {
        // Router before
        this.app.use((err : any, req : any, res : any, next : any) => {
            if (err && err.status === 400 && 'body' in err) {
                return cdg.api(res, new Promise((resolve) => {
                    resolve({
                        status: Define.badRequestErrorCode,
                        message: 'Votre requête contient une ou plusieurs erreurs',
                        data: null
                    });
                }));
            }

            next();
        });

        // Router while
        setRoutes(this.app);

        // Router - after - 404 not found error handler
        this.app.use((err : any, res : any) => {
            return cdg.api(res, new Promise((resolve) => {
                resolve({status: Define.notfoundErrorCode, message: 'Route introuvable ou inexistante', data: null});
            }));
        });
        // End Router
    }

    private listen():void {
        // Default port set
        let port = config.server.port || Define.defaultPort;

        this.app.listen(port, () => {
            cdg.konsole("Serveur connecté! :: port " + port);
            this.db().then(async () => {
                // this.transporter();
                await UserHelper.buildDefaultUser()
            })
        }).on('error', (err: any) => {
            console.log('ERROR::', err.code);
            if(err.code === 'EADDRINUSE') {
                cdg.konsole(`Port ${port} est déjà utilisé`, 1);
            } else {
                cdg.konsole(err, 1);
            }
        });
    }

    private db() {
        return new Promise((resolve, reject) => {
            const uriDb = config.mongo.uri! + config.mongo.name! + '?authSource=admin';
            const db = new Mongoose(uriDb, 'MongoDB connecté!');
            db.connect((Q : any) => {
                if (Q.status === 0) {
                    cdg.konsole(Q.data);
                    resolve(Q)
                } else {
                    cdg.konsole(Q.data, 1);
                    reject(Q)
                }
            });
        }).catch((err) => {
            return {error: true, data: err}
        })
    }
}

export default new App().app;
