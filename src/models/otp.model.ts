import {Schema, model} from 'mongoose';
import {OtpInterface} from "./interface";
import {MongoDbSet} from "./sets/mongodb.set";


// 1. Create a Schema corresponding to the document interface.
const schema = new Schema<OtpInterface>({
    _id: Schema.Types.ObjectId,
    code: {
        type: String,
        required: true
    }, // Code généré
    status: {
        type: String,
        default: 'active' // active - used - expired
    }, //
    object: String, //
    user: String, //
}, {timestamps: true});

/*
|-------------------------------------------- 
| CREATE MODEL
|--------------------------------------------
*/
const Model = model < OtpInterface > ('otpDoc', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY EXPORTED CLASS
|--------------------------------------------
*/
export class OtpSet extends MongoDbSet {
    static defaultModel = Model;
    constructor() {
        super()
    }
}
