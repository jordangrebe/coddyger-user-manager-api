export interface PasswordInterface {
    password?: string;
    passwordData?: object;
    status?: string;
    user?: string;
}

export interface PasswordInputInterface {
    email?: string;
    password?: string;
    passwordConf?: string;
}

export interface PasswordEditInputInterface {
    email: string;
    password: string;
    passwordNew: string;
    passwordConf: string;
}

export interface PasswordScoreInterface {
    score: number,
    message: string,
}
