export interface OtpInterface {
    _id?: string,
    code: string,
    user: string,
    object: string,
    status?: string,
}
