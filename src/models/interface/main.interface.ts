export interface MainInterface {
    _id?: string,

    status?: string,
    user?: string
}

export interface ProfileInterface {
    _id?: string,
    title: string,
    code: string,
    ability: Array < any >,
    status?: string, //
    bucket?: string,
    user?: string
}

export interface UserInterface {
    _id?: string,
    email: string,
    contact: string,
    password: any,
    passwordConfirmation: string,
    lastLoginDate?: Date,
    type: string,
    role: string,
    data?: object // User personal or professional informations

    status?: string, //
    profile: string,
    user?: string
}

export interface LoginInterface {
    email: string,
    password: string | number
}
