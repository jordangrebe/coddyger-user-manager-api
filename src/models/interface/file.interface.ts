export interface MulterFileInterface {
    path: string,
    mimetype: string,
    filename: string,
    originalname: string,
    encoding: string,
    destination: string,
    size: number,
    uri?: string,
}
