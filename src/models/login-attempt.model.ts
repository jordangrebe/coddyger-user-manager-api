import mongoose from "mongoose";
import { MongoDbSet } from "./sets/mongodb.set";

const schema = new mongoose.Schema({
    attempt: Number,
    object: String,
    user: String,
    status: {
        type: String,
        default: 'active' // active / archived / removed (Le compte n'est plus accéssible) / locked
    }
},{timestamps: true});
/*
|--------------------------------------------
| EXPORT MODEL FOR USAGE
|--------------------------------------------
*/
export const model = mongoose.model('loginAttemptDoc', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY MAINSET
|--------------------------------------------
*/
export class LoginAttemptSet extends MongoDbSet{
    static defaultModel = model;
    constructor(){
     super()   
    }
}
