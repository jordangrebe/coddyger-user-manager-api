import mongoose, { Schema, model } from 'mongoose';
import { MongoDbSet } from "./sets/mongodb.set";
import { cdg } from '../utils';
import { LoggerCore } from '../core';
import { locale } from '../data';

// 1. Create a Schema corresponding to the document interface.
const schema = new Schema({
    _id: Schema.Types.ObjectId,
    email: { type: String, required: true, unique: true },
    contact: { type: String, unique: true },
    password: { type: String },
    lastLoginDate: { type: Date },
    status: { type: String, enum: ['active', 'suspended', 'removed'], default: 'active' },
    role: { type: String, enum: ['admin', 'user', 'guest'], default: 'admin' },
    profile: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
    data: {type: Object}, // User personal or professional informations
    ability: Array,
  }, {timestamps: true});

/*
|-------------------------------------------- 
| CREATE MODEL
|--------------------------------------------
*/
const Model = model('User', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY EXPORTED CLASS
|--------------------------------------------
*/
export class UserSet extends MongoDbSet{
    static defaultModel = Model;
    constructor(){
     super()   
    }

    static selectOne(params : Object) {
        return new Promise(async (resolve, reject) => {
            let Q: any = await this.defaultModel.findOne(params).populate("profile");

            if (Q == null) {
                reject(Q)
            } else {
                resolve(Q)
            }
        }).catch((e : any) => {
          LoggerCore.log({type: 'error', content: e, location: 'UserSet', method: 'select'});
            return {error: true, data: e};
        });
    }

    static select(query : {
        params?: any,
        excludes?: any,
        sort?: any
    }) {
        return new Promise(async (resolve, reject) => {
            query.params = (cdg.string.is_empty(query.params) ? {} : query.params);
            query.excludes = (cdg.string.is_empty(query.excludes) ? {} : query.excludes);
            query.sort = (cdg.object.is_empty(query.sort) ? {
                createdAt: '-1'
            } : query.sort);

            let doc = await this.defaultModel.find(query.params, query.excludes, query.sort).lean().populate("profile")

            if (!doc) {
                reject(doc);
            } else {
                resolve(doc)
            }
        }).catch((e : any) => {
            LoggerCore.log({type: 'error', content: e, location: 'UserSet', method: 'select'});
            return {error: true, data: e, message: locale.system.errorTryCatchMessage};
        });
    }

}
