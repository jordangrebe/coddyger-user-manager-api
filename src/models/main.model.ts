import { Schema, model } from 'mongoose';
import { MongoDbSet } from "./sets/mongodb.set";

// 1. Create a Schema corresponding to the document interface.
const schema = new Schema({
    _id: Schema.Types.ObjectId,
    ref: String,
    
    status: {
        type: String,
        default: 'active'
    }, //
    user:  {type: String}, //
  }, {timestamps: true});

/*
|-------------------------------------------- 
| CREATE MODEL
|--------------------------------------------
*/
const MainModel = model('mainDoc', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY EXPORTED CLASS
|--------------------------------------------
*/
export class MainSet extends MongoDbSet{
    static defaultModel = MainModel;
    constructor(){
     super()   
    }
}
