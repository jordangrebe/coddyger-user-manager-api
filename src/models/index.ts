/*
|--------------------------------------------
| EXPORT MODEL FOR USAGE
|--------------------------------------------
*/
export * from './main.model'
export * from './user.model'
export * from './profile.model'
export * from './otp.model'
export * from './password.model'
