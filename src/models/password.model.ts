import mongoose, { Schema } from "mongoose";
import { PasswordInterface } from "./interface";
import { MongoDbSet } from "./sets/mongodb.set";

const schema = new mongoose.Schema<PasswordInterface>({
    password:  {type: String},
    passwordData:  {type: Object},
    status: {
        type: String,
        default: 'active'
    },
    user:  {type: Schema.Types.ObjectId, ref: 'mainDoc'}, //
},
{timestamps: true}); 
/*
|-------------------------------------------- 
| CREATE MODEL
|--------------------------------------------
*/
const model = mongoose.model<PasswordInterface>('passwordDoc', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY EXPORTED CLASS
|--------------------------------------------
*/
export class PasswordSet extends MongoDbSet{
    static defaultModel = model
    
    constructor(){
     super()   
    }
}
