import mongoose from "mongoose";
import { MongoDbSet } from "./sets/mongodb.set";

const schema = new mongoose.Schema({
    title:  {type: String},
    code:  {type: String},
    ability: {
        type: Array,
        default: []
    },
    status: { type: String, enum: ['active', 'inactive', 'suspended', 'removed'], default: 'active' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
},
{timestamps: true}); 
/*
|-------------------------------------------- 
| CREATE MODEL
|--------------------------------------------
*/
const model = mongoose.model('Profile', schema);
/*
|--------------------------------------------
| DEFAULT METHODS CALLED BY EXPORTED CLASS
|--------------------------------------------
*/
export class ProfileSet extends MongoDbSet{
    static defaultModel = model
    
    constructor(){
     super()   
    }
}
